import { createRouter, createWebHistory } from 'vue-router'
import Dashboard from './views/Dashboard.vue'
import Character from './views/Character.vue'
import About from './views/About.vue'
import Help from './views/Help.vue'

const routes = [
	{
		path: '/',
		name: 'home',
		props: true,
		component: Dashboard
	},
	{
		path: '/character/:charname',
		name: 'character',
		props: true,
		component: Character
	},
	{
		path: '/about',
		name: 'about',
		component: About
	},
	{
		path: '/help',
		name: 'help',
		component: Help
	}
]
const router = createRouter({
	history: createWebHistory(),
	routes
})

export default router