import {handleError} from "vue";

export class GetApiData {
	BASE_URL = "https://api.guildwars2.com/v2"

	constructor(apikey) {
		this.apikey = apikey
	}

	async fetch(url, params= {}) {
		params["access_token"] = this.apikey
		params = '?' + ( new URLSearchParams( params ) ).toString()
		return fetch(this.BASE_URL + url + params)
			.then((response) => response.json())
	}

	async getAccount() {
		this.account = await this.fetch("/account")
	}

	async getCharacters() {
		this.characters = await this.fetch("/characters")
	}

	async getCharacter(name) {
		this.character = await this.fetch(`/characters/${name}`)
		this.heropoints = await this.fetch(`/characters/${name}/heropoints`)
		this.title = await this.getCharacterTitle(this.character.title)
	}

	async getContinents() {
		this.continents = await this.fetch("/continents")
	}

	async getCharacterTitle(id) {
		if (id) {
			return fetch(this.BASE_URL + "/titles?id=" + id)
				.then((response) => response.json())
		} else {
			return ''
		}
	}
}