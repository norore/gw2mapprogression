# gw2mapprogression

Application to follow map progression on Guild Wars II for each character of the account.

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

## How to use

### For users that care about their privacy

The application will interact with Guild Wars II API. In the aim to use it, you may have to autorize api.guildwars2.com CDN.

The API key is set in a localstorage with JavaScript, therefore your key is not saved on application server. You can simply delete the localstorage if you don't trust this application.

### API key setup

Go to your ArenaNet account, tab Applications, and generate a new API key with at least:
- account
- characters
- progression

Then, copy-paste your API key on the form in the header of this application.